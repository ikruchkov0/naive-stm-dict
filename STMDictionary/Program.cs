﻿using System;
using System.Threading.Tasks;

namespace STMDictionary
{
    delegate void ApplyFunc<TK, TV>(TransactionContext<TK, TV> context);

    delegate bool DictAccessorFunc<in TK, TV>(TK key, out TV value);

    class Program
    {
        static void WithConflict(int testKey, StmDictionary<int, string> dict)
        {
            var tcs = new TaskCompletionSource<int>(); // uses to synchronize start for tasks

            var task1 = Task.Run(async () =>
            {
                var key = await tcs.Task; // wait for start
                dict.Change(context =>
                {
                    if (!context.TryGetValue(key, out var val))
                    {
                        context.Set(key, "1");
                        return;
                    }
                    context.Set(key, "Old value: " + val);
                });
            });

            var task2 = Task.Run(async () =>
            {
                var key = await tcs.Task; // wait for start
                dict.Change(context =>
                {
                    if (!context.TryGetValue(key, out var val))
                    {
                        context.Set(key, "2");
                        return;
                    }
                    context.Set(key, "Old value: " + val);
                });
            });

            tcs.SetResult(testKey); // start dict changes
            Task.WaitAll(task1, task2); // wait for tasks

            string data;
            dict.TryGetValue(testKey, out data);
            Console.WriteLine("Written data with conflict {0}", data);
        }

        static void WithoutConflict(int testKey, StmDictionary<int, string> dict)
        {
            var tcs = new TaskCompletionSource<int>(); // uses to synchronize start for tasks

            var task1 = Task.Run(async () =>
            {
                var key = await tcs.Task; // wait for start
                dict.Change(context =>
                {
                    if (!context.TryGetValue(key, out var val))
                    {
                        context.Set(key, "1");
                        return;
                    }
                    context.Set(key, "Old value: " + val);
                });
            });

            var task2 = Task.Run(async () =>
            {
                await task1; // wait first task
                var key = await tcs.Task; // wait for start
                dict.Change(context =>
                {
                    if (!context.TryGetValue(key, out var val))
                    {
                        context.Set(key, "2");
                        return;
                    }
                    context.Set(key, "Old value: " + val);
                });
            });

            tcs.SetResult(testKey); // start dict changes
            Task.WaitAll(task1, task2); // wait for tasks

            string data;
            dict.TryGetValue(testKey, out data);
            Console.WriteLine("Written data without conflict {0}", data);
        }

        static void Main(string[] args)
        {
            var dict = new StmDictionary<int, string>();

            WithConflict(11, dict);
            WithoutConflict(12, dict);
            Console.ReadLine();
        }
    }
}
