﻿using System.Collections.Generic;

namespace STMDictionary
{
    /// <summary>
    /// Represents transaction context to access to shared memory data
    /// </summary>
    /// <typeparam name="TK"></typeparam>
    /// <typeparam name="TV"></typeparam>
    class TransactionContext<TK, TV>
    {
        /// <summary>
        /// Lock object for read set. Prevents access to transaction scope from multiple threads
        /// </summary>
        private readonly object _readsLocker = new object();

        /// <summary>
        /// Lock object for write set. Prevents access to transaction scope from multiple threads
        /// </summary>
        private readonly object _writesLocker = new object();

        /// <summary>
        /// Access to shared memory data
        /// </summary>
        private readonly DictAccessorFunc<TK, TV> _dictAccessor;

        /// <summary>
        /// Read set. Used to remembers reads for check on commitment phase
        /// </summary>
        private readonly IDictionary<TK, TV> _reads;

        /// <summary>
        /// Write set. Used to remembers reads for check on commitment phase
        /// </summary>
        private readonly IDictionary<TK, TV> _writes;

        public TransactionContext(DictAccessorFunc<TK, TV> dictAccessor, IDictionary<TK, TV> reads, IDictionary<TK, TV> writes, Conflict<TK, TV> conflict)
        {
            _dictAccessor = dictAccessor;
            _reads = reads;
            _writes = writes;
            Conflict = conflict;
        }

        /// <summary>
        /// Last conflict data. Could be null if there is no conflict
        /// </summary>
        public Conflict<TK, TV> Conflict { get; }

        /// <summary>
        /// Reads value from the transaction scope.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TK key, out TV value)
        {
            // try to get value from transaction scope
            // goes first to overwrite data in read cache and in the shared data
            lock (_writesLocker)
            {
                if (_writes.TryGetValue(key, out value))
                {
                    return true;
                }
            }

            // try to get value from cached read data to reduce lock time of shared memory data
            lock (_readsLocker)
            {
                if (_reads.TryGetValue(key, out value))
                {
                    return true;
                }
            }

            // try to get value from shared memory data
            if (!_dictAccessor(key, out value))
            {
                return false;
            }

            // update cache
            lock (_readsLocker)
            {
                _reads[key] = value;
            }
            return true;
        }

        /// <summary>
        /// Sets value in the transaction scope
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Set(TK key, TV value)
        {
            lock (_writesLocker)
            {
                _writes[key] = value;
            }
        }

        /// <summary>
        /// Clears transaction scope
        /// </summary>
        public void Rollback()
        {
            lock (_writesLocker)
            {
                _writes.Clear();
            }

            lock (_readsLocker)
            {
                _reads.Clear();
            }
        }
    }
}