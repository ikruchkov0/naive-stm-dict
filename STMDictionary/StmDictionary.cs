﻿using System;
using System.Collections.Generic;

namespace STMDictionary
{
    /// <summary>
    /// Naive implementation of software transaction memory for Dictionary
    /// Doesn't support remove
    /// </summary>
    /// <typeparam name="TK"></typeparam>
    /// <typeparam name="TV"></typeparam>
    class StmDictionary<TK, TV>
    {
        /// <summary>
        /// Max count for transaction body reruns
        /// </summary>
        private const int MAX_TRANSACTION_ATTEMPTS = 50;

        /// <summary>
        /// Locker object for shared memory data
        /// </summary>
        private readonly object _locker = new object();

        /// <summary>
        /// Global revision of shared memory data
        /// </summary>
        private int _revision = 0;

        /// <summary>
        /// Shared memory data
        /// </summary>
        private readonly Dictionary<TK, TV> _dict;

        /// <summary>
        /// Revision of values in the shared memory data
        /// </summary>
        private readonly Dictionary<TK, int> _revisions;

        public StmDictionary()
        {
            _revisions = new Dictionary<TK, int>();
            _dict = new Dictionary<TK, TV>();
        }

        /// <summary>
        /// Read accessor to shared memory data
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TK key, out TV value)
        {
            lock (_locker)
            {
                return _dict.TryGetValue(key, out value);
            }
        }

        /// <summary>
        /// Applies transaction on shared memory data
        /// </summary>
        /// <param name="apply">Body of transaction</param>
        public void Change(ApplyFunc<TK, TV> apply)
        {
            var attemptNumber = 0;
            Conflict<TK, TV> lastConflict = null;

            // prevent infinite loop in case of deadlock
            while (attemptNumber < MAX_TRANSACTION_ATTEMPTS)
            {
                // get revision for changeset
                var changeRevision = _revision + 1;
                // initialize transaction context
                var reads = new Dictionary<TK, TV>();
                var writes = new Dictionary<TK, TV>();
                var context = new TransactionContext<TK, TV>(TryGetValue, reads, writes, lastConflict);
                // run transaction body
                apply(context);
                // try to commit
                lastConflict = Commit(changeRevision, attemptNumber, reads, writes);
                if (lastConflict == null)
                {
                    // transcation was applied
                    break;
                }
                Console.WriteLine("Conflict {0}", lastConflict);
                attemptNumber++;
            }
        }

        private Conflict<TK, TV> Commit(int changeRevision, int attemptNumber, IDictionary<TK, TV> reads, IDictionary<TK, TV> writes)
        {
            // lock both revisions and shared memory
            lock (_locker)
            {
                // check reads first to keep data consistent
                var readConflict = CheckReads(changeRevision, attemptNumber, reads);
                if (readConflict != null)
                {
                    return readConflict;
                }

                // try to apply writes
                var writeConflict = ApplyWrites(changeRevision, attemptNumber, writes);
                if (writeConflict != null)
                {
                    return writeConflict;
                }

                // increment global revision
                _revision++;
                return null;
            }
        }

        /// <summary>
        /// Apply writes with check in the transaction scope
        /// </summary>
        /// <param name="changeRevision"></param>
        /// <param name="attemptNumber"></param>
        /// <param name="writes"></param>
        /// <returns></returns>
        private Conflict<TK, TV> ApplyWrites(int changeRevision, int attemptNumber, IDictionary<TK, TV> writes)
        {
            foreach (var write in writes)
            {
                if (_revisions.TryGetValue(write.Key, out var revision))
                {
                    // check revision and return conflict if something was changed
                    if (revision >= changeRevision)
                    {
                        return new Conflict<TK, TV>(revision, write.Key, _dict[write.Key], changeRevision, write.Value, attemptNumber);
                    }
                }
                // update revision of values and values
                _dict[write.Key] = write.Value;
                _revisions[write.Key] = changeRevision;
            }
            return null;
        }

        /// <summary>
        /// Checks reads in transaction scope
        /// </summary>
        /// <param name="changeRevision"></param>
        /// <param name="attemptNumber"></param>
        /// <param name="reads"></param>
        /// <returns></returns>
        private Conflict<TK, TV> CheckReads(int changeRevision, int attemptNumber, IDictionary<TK, TV> reads)
        {
            foreach (var read in reads)
            {
                if (_revisions.TryGetValue(read.Key, out var revision))
                {
                    if (revision >= changeRevision)
                    {
                        return new Conflict<TK, TV>(revision, read.Key, _dict[read.Key], changeRevision, read.Value, attemptNumber);
                    }
                }
            }
            return null;
        }
    }
}