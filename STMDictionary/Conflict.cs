﻿using System.Text;

namespace STMDictionary
{
    /// <summary>
    /// Represents conflict description
    /// </summary>
    /// <typeparam name="TV"></typeparam>
    class Conflict<TK, TV>
    {
        public Conflict(int currentRevision, TK key, TV currentValue, int changeRevision, TV changeValue, int attemptNumber)
        {
            CurrentRevision = currentRevision;
            CurrentValue = currentValue;
            ChangeRevision = changeRevision;
            ChangeValue = changeValue;
            AttemptNumber = attemptNumber;
            Key = key;
        }

        /// <summary>
        /// Key causes conflict
        /// </summary>
        public TK Key { get; }

        /// <summary>
        /// Current revision of data written to shared memory
        /// </summary>
        public int CurrentRevision { get; }

        /// <summary>
        /// Current value of data written to shared memory
        /// </summary>
        public TV CurrentValue { get; }

        /// <summary>
        /// Next revision number
        /// </summary>
        public int ChangeRevision { get; }

        /// <summary>
        /// Next value for shared memory data
        /// </summary>
        public TV ChangeValue { get; }

        /// <summary>
        /// How many attempts attempts has been restarted
        /// </summary>
        public int AttemptNumber { get; }

        #region Overrides of Object

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Key {0}\n", Key);
            sb.AppendFormat("CurrentRevision {0}\n", CurrentRevision);
            sb.AppendFormat("CurrentValue {0}\n", CurrentValue);
            sb.AppendFormat("ChangeRevision {0}\n", ChangeRevision);
            sb.AppendFormat("ChangeValue {0}\n", ChangeValue);
            sb.AppendFormat("AttemptNumber {0}", ChangeValue);
            return sb.ToString();
        }

        #endregion
    }
}